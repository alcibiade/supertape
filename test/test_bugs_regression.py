from unittest import TestCase

from tape.basic.minification import minify_basic
from tape.basic.preprocess import preprocess_basic


class TestRegressionBugs(TestCase):
    def test_picture_driver(self):
        basic = '''  
                   CLS0
                   X=0:Y=0:C=0
            LOOP:  READ C
                   READ L
                   IF L=0 THEN END
                   FOR I=1 TO L
                   IF C>0 THEN SET(X,Y,C)
                   X=X+1
                   IF X=80 THEN X=0:Y=Y+1
                   NEXT I
                   GOTO LOOP
        '''

        self.assertEqual('''1CLS0
2X=0:Y=0:C=0
3READC
4READL
5IFL=0THENEND
6FORI=1TOL
7IFC>0THENSET(X,Y,C)
8X=X+1
9IFX=80THENX=0:Y=Y+1
10NEXTI
11GOTO3
''', minify_basic(preprocess_basic(basic)))
