import logging
from unittest import TestCase

from mockups.listeners import TapeFileListenerStub
from tape.audio.file_in import FileInput
from tape.audio.modulation import AudioDemodulator
from tape.file.block import BlockParser
from tape.file.bytes import ByteDecoder
from tape.file.tapefile import TapeFileLoader


class TestAsmFileLoad(TestCase):
    def test_full_stack(self):
        logging.basicConfig(
            format='%(asctime)-15s - [%(levelname)s] - %(name)s - %(message)s',
            level='DEBUG'
        )

        file_listener = TapeFileListenerStub()
        block_listener = TapeFileLoader([file_listener])
        block_parser = BlockParser([block_listener])
        byte_decoder = ByteDecoder([block_parser])
        demodulation = AudioDemodulator([byte_decoder], rate=44100)
        file_in = FileInput('tapes/assembly-01.wav', [demodulation])
        file_in.run()

        self.assertEqual(1, len(file_listener.files))
        self.assertEqual('TESTASM', file_listener.files[0].fname)
