from unittest import TestCase

from tape.basic.preprocess import preprocess_basic, InvalidLabelError, DuplicateLabelError


class TestPreprocessor(TestCase):
    def test_null_process(self):
        self.assertEqual(preprocess_basic(''), '')
        self.assertEqual(preprocess_basic('10 REM HELLO\n'), '10 REM HELLO\n')

    def test_endline(self):
        self.assertEqual(
            preprocess_basic('10 print "hello"'),
            '10 PRINT "HELLO"\n')

    def test_case_conversion(self):
        self.assertEqual(
            preprocess_basic('10 print "hello"'),
            '10 PRINT "HELLO"\n')

    def test_spaces(self):
        self.assertEqual(
            preprocess_basic(' \t  10 print "HELLO"  '),
            '10 PRINT "HELLO"\n')

    def test_empty_lines(self):
        self.assertEqual(
            preprocess_basic('\n\n10 REM\n   \n20 REM\n\n\n'),
            '10 REM\n20 REM\n')

    def test_comments(self):
        self.assertEqual(
            preprocess_basic('10 REM\n# Comment\n # C2\n20 REM'),
            '10 REM\n20 REM\n')

    def test_implicit_lines(self):
        self.assertEqual(
            preprocess_basic('REM\n# Comment\nREM'),
            '1 REM\n2 REM\n')

        self.assertEqual(
            preprocess_basic('REM\n# Comment\n10 REM\nREM'),
            '1 REM\n10 REM\n11 REM\n')

    def test_line_labels(self):
        self.assertEqual(
            preprocess_basic('PRINT "HELLO"\nLB: PRINT "WORLD !"\nGOTO LB'),
            '1 PRINT "HELLO"\n2 PRINT "WORLD !"\n3 GOTO 2\n')

    def test_empty_label(self):
        with self.assertRaises(InvalidLabelError):
            preprocess_basic(': REM\n')

    def test_invalid_label(self):
        with self.assertRaises(InvalidLabelError):
            preprocess_basic('123: REM\n')

    def test_duplicate_label(self):
        with self.assertRaises(DuplicateLabelError):
            preprocess_basic('A: REM\nA: REM\n')

    def test_process_strings(self):
        # TODO: Actually implement safeguards to avoid transformation in strings

        self.assertEqual(
            preprocess_basic('LBL: REM\nPRINT "GOSUB LBL"\n'),
            '1 REM\n2 PRINT "GOSUB 1"\n')

    def test_process_rem(self):
        # TODO: Actually implement safeguards to avoid transformation in remarks

        self.assertEqual(
            preprocess_basic('LBL: REM\nREM GOSUB LBL\n'),
            '1 REM\n2 REM GOSUB 1\n')
