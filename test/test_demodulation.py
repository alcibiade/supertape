from unittest import TestCase

from mockups.listeners import BitListenerStub
from tape.audio.file_in import FileInput
from tape.audio.modulation import AudioDemodulator


class TestDemodulation(TestCase):
    def test_demodulation(self):
        bitlistener = BitListenerStub(log=False)
        demodulation = AudioDemodulator([bitlistener], rate=44100)
        file_in = FileInput('tapes/program-01.wav', [demodulation])
        file_in.run()

        self.assertEqual('10101010' * 128, bitlistener.text[0:8 * 128])
        self.assertEqual('10101010', bitlistener.text[8 * 128:8 * 129])
        self.assertEqual('00111100', bitlistener.text[8 * 129:8 * 130])
