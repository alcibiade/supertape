import shutil
import tempfile
from unittest import TestCase

from mockups.io_utils import load, save


class TestFileSerialize(TestCase):
    def test_serialization(self):
        # logging.basicConfig(
        #     format='%(asctime)-15s - [%(levelname)s] - %(name)s - %(message)s',
        #     level='DEBUG'
        # )

        for wav in ['tapes/program-01.wav', 'tapes/program-02.wav']:
            outfolder = tempfile.mkdtemp()
            tempaudio = outfolder + '/out.wav'

            # Load a sample file

            file1 = load(wav)

            # Serialize it

            save(tempaudio, file1)

            # Re-load it

            file2 = load(tempaudio)

            shutil.rmtree(outfolder)

            self.assertEqual(file1, file2)
