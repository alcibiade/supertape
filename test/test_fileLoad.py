import logging
from unittest import TestCase

from mockups.listeners import TapeFileListenerStub
from tape.audio.file_in import FileInput
from tape.audio.modulation import AudioDemodulator
from tape.file.api import DataBlock, UnexpectedBlockType
from tape.file.block import BlockParser
from tape.file.bytes import ByteDecoder
from tape.file.tapefile import TapeFileLoader


class TestFileLoad(TestCase):
    def test_full_stack(self):
        logging.basicConfig(
            format='%(asctime)-15s - [%(levelname)s] - %(name)s - %(message)s',
            level='DEBUG'
        )

        file_listener = TapeFileListenerStub()
        block_listener = TapeFileLoader([file_listener])
        block_parser = BlockParser([block_listener])
        byte_decoder = ByteDecoder([block_parser])
        demodulation = AudioDemodulator([byte_decoder], rate=44100)
        file_in = FileInput('tapes/program-01.wav', [demodulation])
        file_in.run()

        self.assertEqual(1, len(file_listener.files))
        self.assertEqual(0x00, file_listener.files[0].fgap)

    def test_inconsistent_blocks(self):
        head = DataBlock(0x00, [0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0, 0, 0, 0, 0, 0, 0x2b])
        data = DataBlock(0x01, [])
        tail = DataBlock(0xFF, [])

        with self.assertRaises(UnexpectedBlockType):
            tapefile_loader = TapeFileLoader([])
            for block in [data]:
                tapefile_loader.process_block(block)

        with self.assertRaises(UnexpectedBlockType):
            tapefile_loader = TapeFileLoader([])
            for block in [tail]:
                tapefile_loader.process_block(block)

        with self.assertRaises(UnexpectedBlockType):
            tapefile_loader = TapeFileLoader([])
            for block in [head, head]:
                tapefile_loader.process_block(block)

        with self.assertRaises(UnexpectedBlockType):
            tapefile_loader = TapeFileLoader([])
            for block in [head, data, tail, data]:
                tapefile_loader.process_block(block)
