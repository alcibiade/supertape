import logging
from unittest import TestCase

from mockups.listeners import BlockListenerStub, ByteListenerStub
from tape.audio.file_in import FileInput
from tape.audio.modulation import AudioDemodulator
from tape.file.api import InvalidBlockType, InvalidCRCError, DataBlock
from tape.file.block import BlockParser, BlockSerializer
from tape.file.bytes import ByteDecoder


class TestBlock(TestCase):
    def test_full_stack_decode(self):
        logging.basicConfig(
            format='%(asctime)-15s - [%(levelname)s] - %(name)s - %(message)s',
            level='DEBUG'
        )

        block_listener = BlockListenerStub()
        block_parser = BlockParser([block_listener])
        byte_decoder = ByteDecoder([block_parser])
        demodulation = AudioDemodulator([byte_decoder], rate=44100)
        file_in = FileInput('tapes/program-01.wav', [demodulation])
        file_in.run()

        self.assertEqual(len(block_listener.blocks), 3)
        self.assertEqual(block_listener.blocks[0].type, 0x00)
        self.assertEqual(block_listener.blocks[1].type, 0x01)
        self.assertEqual(block_listener.blocks[2].type, 0xFF)

    def test_invalid_type(self):
        block_listener = BlockListenerStub()
        block_parser = BlockParser([block_listener])

        with self.assertRaises(InvalidBlockType) as context:
            for byte in [0x3c, 0x02, 0x00, 0x02]:
                block_parser.process_byte(byte)

        self.assertEqual(context.exception.type, 2)

    def test_invalid_crc(self):
        block_listener = BlockListenerStub()
        block_parser = BlockParser([block_listener])

        with self.assertRaises(InvalidCRCError):
            for byte in [0x3c, 0x01, 0x01, 0x00, 0x42]:
                block_parser.process_byte(byte)

    def test_serialization(self):
        l = ByteListenerStub()
        serializer = BlockSerializer([l])
        block = DataBlock(1, [0, 1, 2, 3])
        serializer.process_block(block)
        self.assertEqual(l.values, [0x3c, 0x01, 4, 0, 1, 2, 3, 11, 0x55, 0x55])
