from unittest import TestCase

from mockups.io_utils import load
from tape.repository.repo_db import ZodbRepository


class TestRepositoryDb(TestCase):
    def test_storage(self):
        file1 = load('tapes/program-01.wav')
        file2 = load('tapes/program-02.wav')

        repository = ZodbRepository(repository_file=None, observers=[])
        self.assertEqual(repository.get_tape_files(), [])

        repository.add_tape_file(file1)
        self.assertEqual(repository.get_tape_files(), [file1])

        repository.add_tape_file(file2)
        self.assertEqual(repository.get_tape_files(), [file1, file2])
