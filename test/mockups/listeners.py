from tape.audio.api import BitListener
from tape.file.api import ByteListener, DataBlock, BlockListener, TapeFileListener, TapeFile


class ByteListenerStub(ByteListener):
    def __init__(self):
        self.values = []

    def process_byte(self, value: int):
        self.values.append(value)


class BitListenerStub(BitListener):
    def __init__(self, log=False):
        self.offset = 0
        self.text = ''
        self.log = log
        if self.log:
            print('')

    def __del__(self):
        if self.log:
            print('')

    def process_bit(self, value: int):
        self.text += str(value)
        self.offset += 1

        if self.log:
            print(value, end='\n' if self.offset % 80 == 0 else '')


class BlockListenerStub(BlockListener):
    def __init__(self):
        self.blocks = []

    def process_block(self, block: DataBlock):
        self.blocks.append(block)


class TapeFileListenerStub(TapeFileListener):
    def __init__(self):
        self.files = []

    def process_file(self, file: TapeFile):
        self.files.append(file)
