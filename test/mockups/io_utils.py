from typing import List

from mockups.listeners import TapeFileListenerStub
from tape.audio.file_in import FileInput
from tape.audio.file_out import FileOutput
from tape.audio.modulation import AudioDemodulator, AudioModulator
from tape.file.api import TapeFile
from tape.file.block import BlockParser, BlockSerializer
from tape.file.bytes import ByteDecoder, ByteSerializer
from tape.file.tapefile import TapeFileLoader, TapeFileSerializer


def save(filename: str, file: TapeFile):
    file_out = FileOutput(filename)
    modulator = AudioModulator([file_out], rate=44100)
    byte_listener = ByteSerializer([modulator])
    block_serializer = BlockSerializer([byte_listener])
    tape_serializer = TapeFileSerializer([block_serializer])
    tape_serializer.process_file(file)
    file_out.close()


def load(filename: str) -> TapeFile:
    file_listener = TapeFileListenerStub()
    block_listener = TapeFileLoader([file_listener])
    block_parser = BlockParser([block_listener])
    byte_decoder = ByteDecoder([block_parser])
    demodulation = AudioDemodulator([byte_decoder], 44100)
    file_in = FileInput(filename, [demodulation])
    file_in.run()

    return file_listener.files[0]


def load_bytes(bytes: List[int]) -> TapeFile:
    file_listener = TapeFileListenerStub()
    block_listener = TapeFileLoader([file_listener])
    block_parser = BlockParser([block_listener])

    for b in bytes:
        block_parser.process_byte(b)

    return file_listener.files[0]
