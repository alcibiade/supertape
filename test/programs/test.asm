; ----------------------------
; Programme d'essai assembleur
; ----------------------------

        ORG $4800
        EXC INIT

INIT    LDX  #$0001
        STX  COMPT

DEBUT   CLRA
        LDAB #$30

BRUIT   LDX  COMPT
        EORA #$80
        STAA $BFFF  ; Adresse qui fait
                    ; du bruit

BOUCL   DEX
        BNE  BOUCL
        DECB
        BNE  BRUIT

CHANG    ; Changement de frequence
        LDX  COMPT
        LDAB #$5
        ABX
        STX  COMPT
        CPX  #$200
        BGT  INIT
        BRA  DEBUT

; Donnees
COMPT   DFD  $0

; Fin
