from unittest import TestCase

from mockups.listeners import TapeFileListenerStub
from tape.audio.api import AudioStreamError
from tape.audio.file_in import FileInput
from tape.audio.modulation import AudioDemodulator
from tape.file.block import BlockParser
from tape.file.bytes import ByteDecoder
from tape.file.tapefile import TapeFileLoader


class TestFileLoad(TestCase):
    def test_truncated_start(self):
        file_listener = TapeFileListenerStub()
        block_listener = TapeFileLoader([file_listener])
        block_parser = BlockParser([block_listener])
        byte_decoder = ByteDecoder([block_parser])
        demodulation = AudioDemodulator([byte_decoder], rate=44100)
        file_in = FileInput('tapes/program-01-trunc-start.wav', [demodulation])

        with self.assertRaises(AudioStreamError):
            file_in.run()

        self.assertEqual(0, len(file_listener.files))
        self.assertIsNone(block_parser._buffer)

        file_in = FileInput('tapes/program-01.wav', [demodulation])
        file_in.run()

        self.assertEqual(1, len(file_listener.files))

    def test_truncated_end(self):
        file_listener = TapeFileListenerStub()
        block_listener = TapeFileLoader([file_listener])
        block_parser = BlockParser([block_listener])
        byte_decoder = ByteDecoder([block_parser])
        demodulation = AudioDemodulator([byte_decoder], rate=44100)
        file_in = FileInput('tapes/program-01-trunc-end.wav', [demodulation])

        with self.assertRaises(AudioStreamError):
            file_in.run()
