from unittest import TestCase

from mockups.listeners import ByteListenerStub
from tape.audio.file_in import FileInput
from tape.audio.modulation import AudioDemodulator
from tape.file.bytes import ByteDecoder


class TestByteDecoder(TestCase):
    def test_full_stack(self):
        listener = ByteListenerStub()
        decoder = ByteDecoder([listener])
        demodulation = AudioDemodulator([decoder], rate=44100)
        file_in = FileInput('tapes/program-01.wav', [demodulation])
        file_in.run()

        self.assertEqual([0x3c, 0x00, 0x0F], listener.values[0:3])
        self.assertEqual(
            [0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,
             0, 0, 0, 0, 0, 0, 0x2b],
            listener.values[3:18])

    def test_process_bit(self):
        listener = ByteListenerStub()
        decoder = ByteDecoder([listener])

        # Send 0x00 -> Ignored noise
        decoder.process_bit(0)
        self.assertEqual([], listener.values)
        decoder.process_bit(0)
        self.assertEqual([], listener.values)
        decoder.process_bit(0)
        self.assertEqual([], listener.values)
        decoder.process_bit(0)
        self.assertEqual([], listener.values)
        decoder.process_bit(0)
        self.assertEqual([], listener.values)
        decoder.process_bit(0)
        self.assertEqual([], listener.values)
        decoder.process_bit(0)
        self.assertEqual([], listener.values)
        decoder.process_bit(0)
        self.assertEqual([], listener.values)

        # Send 0x3c -> Block synchronization started
        decoder.process_bit(0)
        self.assertEqual([], listener.values)
        decoder.process_bit(0)
        self.assertEqual([], listener.values)
        decoder.process_bit(1)
        self.assertEqual([], listener.values)
        decoder.process_bit(1)
        self.assertEqual([], listener.values)
        decoder.process_bit(1)
        self.assertEqual([], listener.values)
        decoder.process_bit(1)
        self.assertEqual([], listener.values)
        decoder.process_bit(0)
        self.assertEqual([], listener.values)
        decoder.process_bit(0)
        self.assertEqual([0x3c], listener.values)

        # Send 0xFF as first data block
        decoder.process_bit(1)
        self.assertEqual([0x3c], listener.values)
        decoder.process_bit(1)
        self.assertEqual([0x3c], listener.values)
        decoder.process_bit(1)
        self.assertEqual([0x3c], listener.values)
        decoder.process_bit(1)
        self.assertEqual([0x3c], listener.values)
        decoder.process_bit(1)
        self.assertEqual([0x3c], listener.values)
        decoder.process_bit(1)
        self.assertEqual([0x3c], listener.values)
        decoder.process_bit(1)
        self.assertEqual([0x3c], listener.values)
        decoder.process_bit(1)
        self.assertEqual([0x3c, 0xff], listener.values)
