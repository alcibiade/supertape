from unittest import TestCase

from mockups.io_utils import load_bytes
from tape.assembly.encode import create_assembly_file
from tape.basic.decode import BasicFileParser, BasicDecoder
from tape.basic.encode import BasicFileCompiler, BasicEncoder
from tape.log.dump import dump


class TestBasicFileManagement(TestCase):

    def test_asembly_file_production(self):
        asm = '1 ; Hello'

        file = create_assembly_file('hello', asm, 16000)

        self.assertEqual(file.blocks[0].body, [0x48, 0x45, 0x4C, 0x4C, 0x4F, 0x20, 0x20, 0x20,
                                               0x05, 0x00, 0x01, 0x3E, 0x80, 0x00, 0x00])

        self.assertEqual(file.blocks[1].body, [9, 49, 32, 59, 32, 72, 101, 108, 108, 111, 255])

        self.assertEqual(file.blocks[2].body, [])
