from typing import List

import pyaudio

AUDIO_CHUNKSIZE = 2048
AUDIO_FORMAT = pyaudio.paInt16
AUDIO_CHANNELS = 1

AUDIO_TARGET_LEVEL = 25000


class AudioDevice:

    def __init__(self):
        self.p = pyaudio.PyAudio()

    def __del__(self):
        self.p.terminate()

    def get_sample_rate(self, device: int = None) -> int:
        if device is None:
            device = self.get_default_device()

        device_info = self.p.get_device_info_by_host_api_device_index(0, device)

        # TODO: Remove this hack related to a specific USB audio interface
        if 'USB Audio Device: - (hw:' in device_info['name']:
            return 48000

        return int(device_info.get('defaultSampleRate'))

    def open_stream(self,
                    input: bool = None,
                    output: bool = None,
                    input_device_index: int = None,
                    output_device_index: int = None,
                    stream_callback=None) -> pyaudio.Stream:
        rate = self.get_sample_rate(input_device_index) \
            if input_device_index is not None \
            else self.get_sample_rate(output_device_index)

        return self.p.open(format=AUDIO_FORMAT,
                           channels=AUDIO_CHANNELS,
                           rate=rate,
                           input_device_index=input_device_index,
                           output_device_index=output_device_index,
                           input=input, output=output,
                           stream_callback=stream_callback,
                           frames_per_buffer=AUDIO_CHUNKSIZE)

    def get_default_device(self) -> int:
        info = self.p.get_host_api_info_by_index(0)
        return info.get('defaultInputDevice')

    def get_audio_devices(self) -> List:
        info = self.p.get_host_api_info_by_index(0)
        device_info = [self.p.get_device_info_by_host_api_device_index(0, i)
                       for i in range(0, info.get('deviceCount'))
                       ]

        return [
            [d['index'],
             '%s (Inputs: %d, Outputs: %d)' % (d['name'], d['maxInputChannels'], d['maxOutputChannels'])] for d in
            device_info
        ]


_device = AudioDevice()


def get_device() -> AudioDevice:
    return _device
