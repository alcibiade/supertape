import struct
import wave
from typing import Tuple

from tape.audio.api import AudioSignalListener
from tape.audio.device import *


class FileOutput(AudioSignalListener):
    def __init__(self, filename: str, rate: int = 44100):
        self._filename = filename
        self._wf = wave.open(self._filename, 'wb')
        self._wf.setnchannels(AUDIO_CHANNELS)
        self._wf.setframerate(rate)
        self._wf.setsampwidth(2)

        if AUDIO_FORMAT != pyaudio.paInt16:
            raise AssertionError('Unexpected audio format')

    def process_samples(self, buffer: Tuple[int]):
        format = '%dh' % (len(buffer))
        bytes = struct.pack(format, *buffer)
        self._wf.writeframes(bytes)

    def close(self):
        self._wf.close()
