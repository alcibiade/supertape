import math
from typing import List

from tape.audio.api import AudioSignalListener, AudioLevelListener


class AudioLevelCalculator(AudioSignalListener):
    def __init__(self, listeners: List[AudioLevelListener]):
        self._listeners = []
        self.set_listeners(listeners)

    def set_listeners(self, listeners: List[AudioLevelListener]):
        self._listeners = listeners

    def process_samples(self, bytes: List[int]) -> None:
        levelsum = 0
        count = len(bytes)

        for byte in bytes:
            level = byte / 32768.
            levelsum += level * level

        levelavg = levelsum / count
        levelrms = math.sqrt(levelavg)

        for l in self._listeners:
            l.process_level(levelrms)
