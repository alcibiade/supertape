import struct
import wave
from typing import List

from tape.audio.api import AudioSignalListener
from tape.audio.device import *


class FileInput():
    def __init__(self, filename: str, listeners: List[AudioSignalListener]):
        self._signallisteners = listeners
        self._filename = filename

    def run(self):

        wf = wave.open(self._filename, 'rb')

        while True:
            block = wf.readframes(AUDIO_CHUNKSIZE)

            buflen = len(block) / 2
            if buflen == 0:
                break

            format = '%dh' % (buflen)
            bytes = struct.unpack(format, block)

            for l in self._signallisteners:
                l.process_samples(bytes)

        wf.close()
