import logging
import struct
import time
from threading import Thread
from typing import List

import pyaudio

from tape.audio.api import AudioSignalListener, AudioStreamError
from tape.audio.device import get_device


class AudioInput(Thread):
    def __init__(self, listeners: List[AudioSignalListener], daemon: bool = True, device: int = None):
        super().__init__(daemon=daemon, name='AudioInput')
        self._logger = logging.getLogger('audio.in')
        self._active = True
        self._signallisteners = listeners
        self._device = device

    def set_listeners(self, listeners: List[AudioSignalListener]):
        self._signallisteners = listeners

    def run(self):
        self._logger.info('Scanning audio input')

        stream = get_device().open_stream(
            input_device_index=self._device,
            input=True, output=False,
            stream_callback=self.callback
        )

        stream.start_stream()

        while self._active and stream.is_active():
            time.sleep(1)

        stream.stop_stream()
        stream.close()

    def stop(self):
        self._active = False

    def callback(self, in_data, frame_count, time_info, status):
        if status != 0:
            self._logger.warn('%s %s %s' % (str(frame_count), str(time_info), str(status)))

        format = '%dh' % (frame_count)
        bytes = struct.unpack(format, in_data)

        for l in self._signallisteners:
            try:
                l.process_samples(bytes)
            except AudioStreamError as e:
                self._logger.warning(e)

        return (in_data, pyaudio.paContinue)
