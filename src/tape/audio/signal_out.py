import struct
from threading import Thread
from typing import List, Tuple

from pyaudio import Stream

from tape.audio.api import AudioSignalListener
from tape.audio.device import get_device
from tape.audio.modulation import AudioModulator


class AudioPlayerProgress:

    def __init__(self, progress: int, target: int):
        self.progress = progress
        self.target = target

    def __str__(self):
        return '%d out of %d' % (self.progress, self.target)


class AudioPlayerObserver:

    def on_progress(self, progress: AudioPlayerProgress):
        pass


class AudioPlayer(Thread):
    def __init__(self, bits: List[int], observer: AudioPlayerObserver = AudioPlayerObserver(), device=None):
        super().__init__(daemon=True, name='AudioOutput')
        self._bits = bits
        self._observer = observer
        self._device = device

    def run(self):

        stream = get_device().open_stream(
            output_device_index=self._device,
            input=False, output=True)

        stream_writer = _StreamWriter(stream)
        modulator = AudioModulator([stream_writer], get_device().get_sample_rate(self._device))
        progress = 0
        target = len(self._bits)

        self._observer.on_progress(AudioPlayerProgress(0, target))

        for bit in self._bits:
            modulator.process_bit(bit)
            progress += 1
            if progress % 20 == 0:
                self._observer.on_progress(AudioPlayerProgress(progress, target))

        self._observer.on_progress(AudioPlayerProgress(target, target))

        stream.stop_stream()
        stream.close()


class _StreamWriter(AudioSignalListener):
    def __init__(self, stream: Stream):
        self._stream = stream

    def process_samples(self, buffer: Tuple[int]):
        format = '%dh' % (len(buffer))
        bytes = struct.pack(format, *buffer)
        self._stream.write(bytes)
