from typing import List

from tape.file.api import TapeFile


class TapeFileRepositoryObserver:
    def file_added(self, file: TapeFile):
        pass

    def file_removed(self, file: TapeFile):
        pass


class TapeFileRepository():
    def add_tape_file(self, file: TapeFile) -> None:
        pass

    def get_tape_files(self) -> List[TapeFile]:
        pass

    def add_observer(self, observer: TapeFileRepositoryObserver):
        pass

    def remove_observer(self, observer: TapeFileRepositoryObserver):
        pass
