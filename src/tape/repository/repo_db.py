import copy
from typing import List

from ZODB import DB
from ZODB.FileStorage import FileStorage
from persistent.list import PersistentList

from tape.file.api import TapeFile
from tape.repository.api import TapeFileRepository, TapeFileRepositoryObserver


class ZodbRepository(TapeFileRepository):
    def __init__(self, repository_file: str, observers: List[TapeFileRepositoryObserver]):
        self.repository_file = repository_file
        storage = FileStorage(repository_file + '.fs') if repository_file is not None else None
        self.db = DB(storage)
        self.observers = observers

        with self.db.transaction() as connection:
            if 'files' not in connection.root():
                connection.root.files = PersistentList()

    def __str__(self):
        return 'Zodb at %s' % (self.repository_file)

    def add_tape_file(self, file: TapeFile):
        with self.db.transaction() as connection:
            connection.root.files.append(file)

        for observer in self.observers:
            observer.file_added(file)

    def remove_tape_file(self, file: TapeFile):
        with self.db.transaction() as connection:
            connection.root.files.remove(file)

        for observer in self.observers:
            observer.file_removed(file)

    def get_tape_files(self) -> List[TapeFile]:
        with self.db.transaction() as connection:
            res = copy.deepcopy(connection.root.files)

        return res

    def add_observer(self, observer: TapeFileRepositoryObserver):
        self.observers.append(observer)

    def remove_observer(self, observer: TapeFileRepositoryObserver):
        self.observers.remove(observer)
