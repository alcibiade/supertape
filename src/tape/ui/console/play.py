import argparse
import time

from tape.assembly.encode import create_assembly_file
from tape.audio.signal_out import AudioPlayerObserver, AudioPlayerProgress
from tape.basic.encode import BasicFileCompiler, BasicEncoder
from tape.basic.minification import minify_basic
from tape.basic.preprocess import preprocess_basic
from tape.file.api import TapeFile
from tape.file.play import play_file


def read_program(file: str) -> str:
    with open(file, 'r') as f:
        basic_source = f.read()

    return basic_source


def convert_program_to_tapefile(file_name: str, basic_code: str) -> TapeFile:
    file_compiler = BasicFileCompiler()
    encoder = BasicEncoder()

    instructions = [encoder.encode(l) for l in basic_code.splitlines()]
    outfile = file_compiler.compile_instructions(file_name, instructions)

    return outfile

def convert_assembly_program_to_tapefile(file_name: str, assembly_code: str) -> TapeFile:
    outfile = create_assembly_file(file_name, assembly_code)
    return outfile


class AudioObserver(AudioPlayerObserver):
    def __init__(self):
        self.complete = False

    def on_progress(self, progress: AudioPlayerProgress):
        if progress.progress == progress.target:
            self.complete = True

    def wait_for_audio_completion(self):
        while not self.complete:
            time.sleep(0.5)

        time.sleep(0.5)


def play_tape(device: int, tape_file: TapeFile) -> None:
    obs = AudioObserver()
    play_file(device=device, file=tape_file, observer=obs)
    obs.wait_for_audio_completion()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Play a local file to the audio interface.')
    parser.add_argument('--device', help='Select a device index.', type=int)
    parser.add_argument('file', type=str)
    args = parser.parse_args()

    if args.file[-4:].lower() == '.bas':
        basic_code = read_program(args.file)
        basic_code = preprocess_basic(basic_code)
        basic_code = minify_basic(basic_code)
        tape_file = convert_program_to_tapefile(args.file, basic_code)
    elif args.file[-4:].lower() == '.asm':
        asm_code = read_program(args.file)
        tape_file = convert_assembly_program_to_tapefile(args.file, asm_code)

    play_tape(device=args.device, tape_file=tape_file)
