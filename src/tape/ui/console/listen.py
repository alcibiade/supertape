import argparse

from tape.audio.device import get_device
from tape.audio.file_in import FileInput
from tape.audio.modulation import AudioDemodulator
from tape.audio.signal_in import AudioInput
from tape.basic.decode import BasicFileParser, BasicDecoder
from tape.file.api import ByteListener, TapeFileListener, TapeFile
from tape.file.block import BlockParser, BlockPrinter
from tape.file.bytes import ByteDecoder
from tape.file.tapefile import TapeFileLoader, TapeFilePrinter


class TapeFileLister(TapeFileListener):

    def __init__(self, active: bool):
        self._active = active

    def process_file(self, file: TapeFile):

        if self._active:
            parser = BasicFileParser()
            decoder = BasicDecoder()

            for basic_line in parser.get_binary_instructions(file):
                print('    ', decoder.decode(instruction=basic_line.instruction))


class TapeFileDumper(ByteListener, TapeFileListener):

    def __init__(self, active: bool):
        self._buffer = []
        self._active = active

    def process_byte(self, value: int) -> None:
        if not self._active:
            return

        self._buffer.append(value)

    def process_file(self, file: TapeFile) -> None:
        if not self._active:
            return

        print('[', end='')
        for ix, b in enumerate(self._buffer):
            if ix > 0:
                print(', ', end='')

            if ix % 16 == 15:
                print('')

            print('0x%02x' % (b), end='')

        print(']')
        self._buffer = []


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Listen to an audio interface.')
    parser.add_argument('--device', help='Select a device index.', type=int)
    parser.add_argument('--dump', help='Dump raw file bytes.', action='store_true')
    parser.add_argument('--list', help='Show program listing.', action='store_true')
    parser.add_argument('file', nargs='?', type=str)
    args = parser.parse_args()

    file_printer = TapeFilePrinter()
    block_printer = BlockPrinter()
    file_dumper = TapeFileDumper(active=args.dump)
    file_lister = TapeFileLister(active=args.list)

    file_loader = TapeFileLoader([file_printer, file_dumper, file_lister])
    block_parser = BlockParser([block_printer, file_loader])
    byte_decoder = ByteDecoder([file_dumper, block_parser])

    if args.file is None:
        demodulation = AudioDemodulator([byte_decoder], rate=get_device().get_sample_rate())
        audio_in = AudioInput([demodulation], daemon=False, device=args.device)
        audio_in.start()
    else:
        demodulation = AudioDemodulator([byte_decoder], rate=44100)
        file_in = FileInput(filename=args.file, listeners=[demodulation])
        file_in.run()
