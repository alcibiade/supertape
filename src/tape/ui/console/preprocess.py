import argparse

from tape.basic.minification import minify_basic
from tape.basic.preprocess import preprocess_basic


def read_program(file: str) -> str:
    with open(file, 'r') as f:
        basic_source = f.read()

    return basic_source


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Pre-process a BASIC file.')
    parser.add_argument('--minify', help='Minify the target program.', action='store_true')
    parser.add_argument('file', help='The BASIC code to pre-process', type=str)
    args = parser.parse_args()

    basic_code = read_program(args.file)
    basic_code = preprocess_basic(basic_code)

    if args.minify:
        basic_code = minify_basic(basic_code)

    print(basic_code)
