import array
import pickle
import sys

if __name__ == '__main__':
    pickler = pickle.Unpickler(open('tapes.dat', 'rb'))
    entries = pickler.load()

    if len(sys.argv) == 1:
        ix = 0

        for entry in entries:
            print('%4d: %-8s  - %5d bytes' % (ix, entry.file.fname, len(entry.file.fbody)))
            ix += 1
    else:
        filename = sys.argv[1].upper()
        k7name = filename + '.k7'
        bytes_array = bytes()

        for entry in entries:
            if filename == entry.file.fname:
                bytes_array = array.array('B', entry.file.fbody)

        with open(k7name, 'wb') as k7file:
            k7file.write(bytes_array)
