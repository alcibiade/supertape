import argparse
import pprint

import tabulate

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='List all available audio devices.')
    parser.add_argument('-l', help='Extended device information.', action='store_true')
    args = parser.parse_args()

    import pyaudio
    p = pyaudio.PyAudio()
    info = p.get_host_api_info_by_index(0)
    device_info = [p.get_device_info_by_host_api_device_index(0, i)
                   for i in range(0, info.get('deviceCount'))
                   ]

    if args.l:
        for d in device_info:
            pprint.pprint(d)

    rows = [
        [d['index'], d['name'], d['maxInputChannels'], d['maxOutputChannels'], d['defaultSampleRate']] for d in
        device_info
    ]

    print(
        tabulate.tabulate(rows, headers=['Index', 'Name', 'Input Channels', 'Output Channels', 'Default Sample Rate']))
