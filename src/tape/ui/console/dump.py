import sys

from tape.audio.file_in import FileInput
from tape.audio.modulation import AudioDemodulator
from tape.file.api import ByteListener
from tape.file.bytes import ByteDecoder
from tape.log.dump import dump


class ByteListenerStub(ByteListener):
    def __init__(self):
        self.values = []

    def process_byte(self, value: int):
        self.values.append(value)


if __name__ == '__main__':
    targetfile = sys.argv[1]
    byte_accumulator = ByteListenerStub()

    if targetfile[-4:] == '.wav':
        byte_decoder = ByteDecoder([byte_accumulator])
        demodulation = AudioDemodulator([byte_decoder])
        file_in = FileInput(targetfile, [demodulation])
        file_in.run()

    elif targetfile[-3:] == '.k7':
        with open(targetfile, 'rb') as tape_file:

            while True:
                byte = tape_file.read(1)

                if len(byte) == 0:
                    break

                byte_accumulator.process_byte(byte[0])

    for line in dump(byte_accumulator.values):
        print(line)
