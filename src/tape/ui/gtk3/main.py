import argparse
import logging

import gi

gi.require_version('Gtk', '3.0')

from tape.ui.gtk3.controller import UserInterfaceController
from tape.ui.gtk3.frame.frame import TapeFrame

from gi.repository import Gtk, GObject


def main():
    parser = argparse.ArgumentParser(description='Run a Tape management user interface.')
    parser.add_argument('--device', help='Select a device index.', type=int)
    parser.add_argument('--tapes', help='Tapes storage file to use.', default='tapes')
    args = parser.parse_args()

    logging.basicConfig(
        format='%(asctime)-15s - [%(levelname)s] - %(name)s - %(message)s',
        level='DEBUG'
    )

    controller = UserInterfaceController(device=args.device, storage=args.tapes)

    GObject.threads_init()

    frame = TapeFrame(controller=controller)

    Gtk.main()


if __name__ == '__main__':
    main()
