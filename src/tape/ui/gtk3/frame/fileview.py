import logging

from gi.repository import Gtk

from tape.basic.decode import BasicFileParser, BasicDecoder
from tape.file.api import TapeFile, DataBlock
from tape.file.block import dump_block


class TapeFileViewer(Gtk.Box):
    def __init__(self):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.tape = None
        self._logger = logging.getLogger('ui.fileview')

    def set_tape(self, file: TapeFile):
        self.tape = file
        self._logger.debug('Displaying tape %s' % file)
        for child in self.get_children():
            self.remove(child)
            child.destroy()

        self.pack_start(BasicView(file), False, False, 6)

        for block in file.blocks:
            view = BlockView(block)
            self.pack_start(view, False, False, 6)

        self.show_all()


class BlockView(Gtk.Box):
    def __init__(self, block: DataBlock):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.block = block

        self.text = ''
        dump_block(block, self.append)
        self.text = self.text.strip()

        self.textbuffer = Gtk.TextBuffer()
        self.textbuffer.set_text(self.text, len(self.text))
        self.textview = Gtk.TextView.new_with_buffer(self.textbuffer)
        self.textview.set_monospace(True)
        self.textview.set_editable(False)
        self.pack_start(self.textview, False, False, 0)

    def append(self, text):
        self.text += text + '\n'

    def __str__(self):
        return 'BlockView{%s}' % self.block


class BasicView(Gtk.Box):
    def __init__(self, file: TapeFile):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.file = file

        parser = BasicFileParser()
        decoder = BasicDecoder()

        text = ''

        for basic_line in parser.get_binary_instructions(file):
            text += decoder.decode(instruction=basic_line.instruction) + '\n'

        self.textbuffer = Gtk.TextBuffer()
        self.textbuffer.set_text(text, len(text))
        self.textview = Gtk.TextView.new_with_buffer(self.textbuffer)
        self.textview.set_monospace(True)
        self.textview.set_editable(False)
        self.pack_start(self.textview, False, False, 0)

    def __str__(self):
        return 'BasicView{%s}' % self.file.fname
