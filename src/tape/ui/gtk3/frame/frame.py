import logging

from gi.repository import Gtk, GLib, Gdk

from tape.audio.api import AudioLevelListener
from tape.file.load import file_load
from tape.ui.gtk3.controller import UserInterfaceController
from tape.ui.gtk3.frame.fileview import TapeFileViewer
from tape.ui.gtk3.frame.player import ModalPlayer
from tape.ui.gtk3.tapelist.repository import TapeRepositoryWrapperStore
from tape.ui.gtk3.tapelist.view import TapeView


class AudioLevelManager(AudioLevelListener):

    def __init__(self, progress_bar: Gtk.ProgressBar):
        self._logger = logging.getLogger('ui.level')
        self._bar = progress_bar

    def process_level(self, level: float) -> None:
        # self._logger.debug('Audio level set to %f' % (level))
        GLib.idle_add(self._bar.set_fraction, level)


class TapeFrame(Gtk.Window):
    def __init__(self, controller: UserInterfaceController):
        Gtk.Window.__init__(self, title="Smart Tape Recorder")
        self._logger = logging.getLogger('ui.frame')
        self._controller = controller

        self.set_border_width(10)
        self.set_gravity(Gdk.Gravity.CENTER)
        self.connect("delete-event", Gtk.main_quit)

        mainbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.add(mainbox)

        mainbox.pack_start(self._build_device_selector(), False, False, 0)
        mainbox.pack_start(self._build_audio_level(), False, False, 0)

        sideboxes = Gtk.Paned(orientation=Gtk.Orientation.HORIZONTAL)
        sideboxes.add1(self._build_tape_view())
        sideboxes.add2(self._build_file_view())

        mainbox.pack_start(sideboxes, True, True, 0)

        mainbox.pack_start(self._build_action_box(), False, False, 0)

        self.set_default_size(640, 480)

        self._logger.debug('Displaying frame')
        self.show_all()

    def _build_tape_view(self) -> Gtk.Widget:
        self.tape_store = TapeRepositoryWrapperStore(self._controller.get_repository())
        self.tape_view = TapeView(self.tape_store)
        icon_renderer = Gtk.CellRendererPixbuf()
        icon_column = Gtk.TreeViewColumn('', icon_renderer, icon_name=0)
        self.tape_view.append_column(icon_column)
        name_renderer = Gtk.CellRendererText()
        name_column = Gtk.TreeViewColumn('Program Name', name_renderer, text=1)
        self.tape_view.append_column(name_column)
        type_renderer = Gtk.CellRendererText()
        type_column = Gtk.TreeViewColumn('Type', type_renderer, text=2)
        self.tape_view.append_column(type_column)
        size_renderer = Gtk.CellRendererText()
        size_column = Gtk.TreeViewColumn('Size', size_renderer, text=3)
        self.tape_view.append_column(size_column)

        scroll_tape_view = Gtk.ScrolledWindow()
        scroll_tape_view.add(self.tape_view)
        scroll_tape_view.set_size_request(240, -1)
        self.tape_view.connect('cursor-changed', self.on_cursor_changed)
        return scroll_tape_view

    def _build_file_view(self) -> Gtk.Widget:
        self.file_viewer = TapeFileViewer()
        scroll_file_view = Gtk.ScrolledWindow()
        scroll_file_view.add(self.file_viewer)
        return scroll_file_view

    def _build_action_box(self) -> Gtk.Widget:
        actionbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        add_button = Gtk.Button(label=None, image=(Gtk.Image(stock=Gtk.STOCK_ADD)))
        add_button.connect("clicked", self.on_add_clicked)
        play_button = Gtk.Button(label=None, image=(Gtk.Image(stock=Gtk.STOCK_MEDIA_PLAY)))
        play_button.connect("clicked", self.on_play_clicked)
        del_button = Gtk.Button(label=None, image=(Gtk.Image(stock=Gtk.STOCK_DELETE)))
        del_button.connect("clicked", self.on_del_clicked)
        actionbox.pack_start(add_button, False, False, 0)
        actionbox.pack_start(play_button, False, False, 0)
        actionbox.pack_start(del_button, False, False, 0)
        return actionbox

    def _build_audio_level(self) -> Gtk.Widget:
        self.progressbar = Gtk.ProgressBar()
        self.audio_level_manager = AudioLevelManager(self.progressbar)
        self._controller.register_level_listener(self.audio_level_manager)
        return self.progressbar

    def _build_device_selector(self) -> Gtk.Widget:
        device_store = Gtk.ListStore(int, str)
        devices = self._controller.get_audio_devices()
        for (dev_id, dev_name) in devices:
            device_store.append([dev_id, dev_name])
        self.device_selector = Gtk.ComboBox.new_with_model(device_store)
        renderer_text = Gtk.CellRendererText()
        renderer_id = Gtk.CellRendererText()
        self.device_selector.pack_start(renderer_id, False)
        self.device_selector.pack_start(renderer_text, True)
        self.device_selector.add_attribute(renderer_id, "text", 0)
        self.device_selector.add_attribute(renderer_text, "text", 1)
        self.device_selector.set_active(self._controller.get_selected_device())
        self.device_selector.connect("changed", self.on_device_changed)
        return self.device_selector

    def on_add_clicked(self, button):
        dialog = Gtk.FileChooserDialog("Load a tape file", self,
                                       Gtk.FileChooserAction.OPEN,
                                       (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                        Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

        response = dialog.run()

        if response == Gtk.ResponseType.OK:
            file_name = dialog.get_filename()
            tape = file_load(file_name)

            if tape is None:
                errdialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.ERROR,
                                              Gtk.ButtonsType.CLOSE, "Tape loading error")
                errdialog.format_secondary_text("The tape file could not be opened.")
                errdialog.run()
                errdialog.destroy()
            else:
                self._logger.debug('Adding ' + str(tape))
                self.tape_store.add(tape)

        dialog.destroy()

    def on_device_changed(self, combobox):
        selection = combobox.get_active()
        self._logger.debug('Setting device to %d' % selection)
        self._controller.set_selected_device(selection)

    def on_play_clicked(self, button):
        selection = self.tape_view.get_selection()
        model, treeiter = selection.get_selected()
        if treeiter is None:
            return
        tape = model[treeiter][4]

        dialog = ModalPlayer(window=self, tape=tape)
        dialog.show()

        self._logger.debug('Playing %s' % tape)
        self._controller.play_tape(tape, observer=dialog)

    def on_del_clicked(self, button):
        selection = self.tape_view.get_selection()
        model, treeiter = selection.get_selected()

        if treeiter is None:
            return

        tape = model[treeiter][4]
        self._logger.debug('Removing tape %s' % tape)

        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.QUESTION,
                                   Gtk.ButtonsType.YES_NO, "Do you really want to remove file %s" % tape.fname)
        dialog.format_secondary_text(
            "Removed files cannot be recovered.")
        response = dialog.run()
        if response == Gtk.ResponseType.YES:
            self._controller.remove_tape(tape)

        dialog.destroy()

    def on_cursor_changed(self, tree: Gtk.TreeView):
        selection = self.tape_view.get_selection()
        model, treeiter = selection.get_selected()
        if treeiter is None:
            return

        tape = model[treeiter][4]
        self.file_viewer.set_tape(tape)
