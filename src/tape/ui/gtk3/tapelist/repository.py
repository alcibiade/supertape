from gi.overrides.Gtk import ListStore

from tape.file.api import TapeFile
from tape.repository.api import TapeFileRepository, TapeFileRepositoryObserver


class TapeRepositoryWrapperStore(ListStore, TapeFileRepositoryObserver):
    def __init__(self, repository: TapeFileRepository):
        super().__init__(str, str, str, int, object)
        self.repository = repository
        self.repository.add_observer(self)
        self._load()

    def file_added(self, file: TapeFile):
        self._load()

    def file_removed(self, file: TapeFile):
        self._load()

    def _add_internal(self, tape):
        self.append(['media-floppy', tape.fname, hex(tape.ftype), len(tape.fbody), tape])

    def _load(self):
        self.clear()
        tapes = self.repository.get_tape_files()
        for tape in tapes:
            self._add_internal(tape)
