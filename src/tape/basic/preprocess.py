import re

re_comment = re.compile(r'^#')
re_white_line = re.compile(r'^$')

re_line_number = re.compile(r'^(\d+)\s+')
re_line_label = re.compile(r'^(\S*):\s+')
re_valid_label = re.compile(r'^[A-Z]+$')


class BasicPreprocessingException(Exception):
    pass


class InvalidLabelError(BasicPreprocessingException):
    def __init__(self, label: str, line: int):
        self.label = label
        self.line = line

    def __str__(self):
        return 'Invalid label "%s" at line %d' % (self.label, self.line)


class DuplicateLabelError(BasicPreprocessingException):
    def __init__(self, label: str, line: int):
        self.label = label
        self.line = line

    def __str__(self):
        return 'Duplicate label "%s" at line %d' % (self.label, self.line)


def preprocess_basic(code: str) -> str:
    target = ''
    basic_line_number = 0
    labels = {}

    for original_line_number, line in enumerate(code.splitlines()):
        line = line.upper().strip()
        if re_comment.match(line) or re_white_line.match(line):
            continue

        # Detect line number or label, these are exclusive

        ln_matches = re_line_number.findall(line)
        lb_matches = re_line_label.findall(line)

        # Log the label

        label = lb_matches[0] if lb_matches else None
        line = re.sub(re_line_label, '', line) if label else line

        # Update the line number variable accordingly

        if ln_matches:
            basic_line_number = int(ln_matches[0])
        else:
            basic_line_number += 1
            line = '%d %s' % (basic_line_number, line)

        # From here in the loop, basic line number is defined accurately

        if lb_matches:
            if label in labels.keys():
                raise DuplicateLabelError(label=label,
                                          line=original_line_number + 1)

            if not re_valid_label.match(label):
                raise InvalidLabelError(label=label,
                                        line=original_line_number + 1)

            labels[label] = basic_line_number

        target += line + '\n'

    # Now patch all labels

    for label, basic_line in labels.items():
        target = re.sub(r'(GOTO|GOSUB)\s*(' + label + ')',
                        r'\1 ' + str(basic_line),
                        target)

    return target
