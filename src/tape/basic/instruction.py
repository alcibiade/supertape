from typing import List


class BasicBinaryInstruction:
    def __init__(self, bytes: List[int]):
        self.bytes = bytes

    def get_bytes(self) -> List[int]:
        return self.bytes

    def __str__(self):
        return 'Basic Instruction: %s' % (['0x%02x' % b for b in self.bytes])
