from typing import Iterator

from tape.basic.instruction import BasicBinaryInstruction
from tape.basic.opcodes import stringify
from tape.file.api import TapeFile


class BasicLine:
    def __init__(self, fileoffset: int, instruction: BasicBinaryInstruction):
        self.fileoffset = fileoffset
        self.instruction = instruction


class BasicFileParser:

    @staticmethod
    def get_binary_instructions(file: TapeFile) -> Iterator[BasicLine]:
        line_offset = 0
        line_buffer = []

        body = file.fbody

        for offset, byte in enumerate(body):
            line_buffer.append(byte)

            if byte == 0x00 and len(line_buffer) > 4:
                yield BasicLine(fileoffset=line_offset, instruction=BasicBinaryInstruction(bytes=line_buffer))
                line_offset = None
                line_buffer = []


class BasicDecoder:

    @staticmethod
    def decode(instruction: BasicBinaryInstruction) -> str:
        line_byte_high = instruction.bytes[2]
        line_byte_low = instruction.bytes[3]
        line_number = line_byte_high * 256 + line_byte_low

        text = stringify(instruction.bytes[4:])

        return '%4d %s' % (line_number, text)
