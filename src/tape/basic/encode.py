import os
import re
from typing import List

from tape.basic.instruction import BasicBinaryInstruction
from tape.basic.opcodes import binarize
from tape.file.api import TapeFile, DataBlock


class BasicEncoder:

    def __init__(self):
        self._basicindex = 0x3346
        self.re_line = re.compile('^\s*(\d+)\s*(.*\S)\s*$')

    def encode(self, basicline: str) -> BasicBinaryInstruction:
        bytes = []

        for lno, linst in self.re_line.findall(basicline):
            bin_insruction = binarize(linst)
            self._basicindex += 4 + len(bin_insruction)

            lno = int(lno)
            bytes.append((self._basicindex & 0xFF00) >> 8)
            bytes.append(self._basicindex & 0x00FF)
            bytes.append(int(lno / 256))
            bytes.append(lno % 256)
            bytes += bin_insruction

        return BasicBinaryInstruction(bytes=bytes)


class BasicFileCompiler:

    @staticmethod
    def cleanup_program_name(filename: str) -> str:
        base_name = os.path.split(filename)[-1]
        program_name = os.path.splitext(base_name)[0]

        if len(program_name) > 8:
            program_name = program_name[0:8]

        program_name = program_name.upper()
        return program_name

    @staticmethod
    def compile_instructions(filename: str, lines: List[BasicBinaryInstruction]) -> TapeFile:
        body = []

        filename = BasicFileCompiler.cleanup_program_name(filename)
        filename_bytes = [ord(filename[i]) if i < len(filename) else 0 for i in range(8)]

        for instruction in lines:
            body += instruction.bytes

        body += [0x00, 0x00]

        head = DataBlock(type=0x00,
                         body=filename_bytes + [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x16])

        foot = DataBlock(type=0xFF, body=[])

        blocks = [head]

        while body:
            block_content = body[:255]
            body = body[len(block_content):]

            blocks.append(DataBlock(type=0x01, body=block_content))

        blocks.append(foot)

        return TapeFile(blocks=blocks)
