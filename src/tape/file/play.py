from tape.audio.api import BitListener
from tape.audio.signal_out import AudioPlayer, AudioPlayerObserver
from tape.file.api import TapeFile
from tape.file.block import BlockSerializer
from tape.file.bytes import ByteSerializer
from tape.file.tapefile import TapeFileSerializer


class _bit_accumulator(BitListener):
    def __init__(self):
        self.bits = []

    def process_bit(self, value: int):
        self.bits.append(value)


def play_file(file: TapeFile, observer: AudioPlayerObserver, device: int = None) -> None:
    bit_accumulator = _bit_accumulator()
    byte_accumulator = ByteSerializer([bit_accumulator])
    block_serializer = BlockSerializer([byte_accumulator])
    tape_serializer = TapeFileSerializer([block_serializer])
    tape_serializer.process_file(file)

    audio_output = AudioPlayer(bit_accumulator.bits, observer, device=device)
    audio_output.start()
