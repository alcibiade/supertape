#####################################################################
# Data models
#####################################################################
from typing import List

from persistent import Persistent

from tape.audio.api import AudioStreamError

FILE_TYPE_BASIC = 0x00
FILE_TYPE_MACHINE = 0x02
FILE_TYPE_DATA = 0x04
FILE_TYPE_ASMSRC = 0x05

FILE_DATA_TYPE_BIN = 0x00
FILE_DATA_TYPE_ASC = 0xFF

FILE_GAP_NONE = 0x00
FILE_GAP_GAPS = 0xFF


class DataBlock:
    def __init__(self, type: int, body: List[int]):
        self.type = type
        self.body = body
        self.checksum = (type + len(body) + sum(body)) % 256

    def __repr__(self):
        return str(self)

    def __str__(self):
        return 'Block-' + hex(self.type) + '-' + str([hex(x) for x in self.body])

    def __eq__(self, other):
        return self.type == other.type and self.body == other.body


class TapeFile(Persistent):
    def __init__(self, blocks: List[DataBlock]):
        self.blocks = blocks

        self.fname = self._get_name()
        self.fbody = self._get_body()
        self.ftype = self.blocks[0].body[10]
        self.fdatatype = self.blocks[0].body[11]
        self.fgap = self.blocks[0].body[12]
        self.fstartaddress = self.blocks[0].body[13] * 256 + self.blocks[0].body[14]
        self.floadaddress = self.blocks[0].body[15] * 256 + self.blocks[0].body[16] \
            if len(self.blocks[0].body) > 16 else None

    def _get_name(self):
        fname = ''

        for b in self.blocks[0].body[0:8]:
            fname += chr(b)

        return fname.rstrip()

    def _get_body(self):
        body = []

        for block in self.blocks[1:-1]:
            body += block.body

        return body

    def __eq__(self, other):
        return self.blocks == other.blocks

    def __str__(self):
        return 'TapeFile{' + self.fname + '}'


#####################################################################
# Error management
#####################################################################

class InvalidCRCError(AudioStreamError):
    pass


class InvalidBlockType(AudioStreamError):
    def __init__(self, type: int):
        super(InvalidBlockType, self).__init__(
            'Unknown type for Alice block: ' + hex(type)
        )

        self.type = type


class UnexpectedBlockType(AudioStreamError):
    def __init__(self, received_type: int, expected_type: int):
        super(UnexpectedBlockType, self).__init__(
            'Received block type ' + hex(received_type) \
            + ' while expecting type ' + hex(expected_type)
        )

        self.received_type = received_type
        self.expected_type = expected_type


#####################################################################
# Event management interfaces
#####################################################################

class BlockListener:
    def process_block(self, block: DataBlock):
        pass


class ByteListener:
    def process_byte(self, value: int) -> None:
        pass

    def process_silence(self) -> None:
        pass


class TapeFileListener:
    def process_file(self, file: TapeFile):
        pass
