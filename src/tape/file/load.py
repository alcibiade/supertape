from tape.file.api import TapeFile, TapeFileListener
from tape.file.block import BlockParser
from tape.file.tapefile import TapeFileLoader


class _filelistener(TapeFileListener):
    def __init__(self):
        self.file = None

    def process_file(self, file: TapeFile):
        self.file = file


def file_load(file_name: str) -> TapeFile:
    with open(file_name, 'rb') as tape_file:

        file_listener = _filelistener()
        tape_file_loader = TapeFileLoader([file_listener])
        block_parser = BlockParser([tape_file_loader])

        while True:
            byte = tape_file.read(1)

            if len(byte) == 0:
                break

            block_parser.process_byte(byte[0])

        return file_listener.file
