import string
from typing import List

PRINTABLE = [c for c in string.printable if ord(c) >= 0x20]


def dump(bytes: List[int], width: int = 16) -> List[str]:
    lines = []
    offset = 0

    while offset < len(bytes):
        row_hex = ''
        row_asc = ''

        for o in range(0, width):
            v = bytes[offset + o] if offset + o < len(bytes) else None

            if v is None:
                row_hex += '   '
            else:
                c = chr(v)
                row_hex += '%02X ' % (v)
                row_asc += c if c in PRINTABLE else '.'

        lines.append('%04Xh: %s | %s' % (offset, row_hex, row_asc))

        offset += width

    return lines


def dump_ascii_only(bytes: List[int], width: int = 16) -> List[str]:
    lines = []
    offset = 0

    while offset < len(bytes):
        row_asc = ''

        for o in range(0, width):
            v = bytes[offset + o] if offset + o < len(bytes) else None

            if v is not None:
                c = chr(v)
                row_asc += c if c in PRINTABLE else '.'

        lines.append(row_asc)

        offset += width

    return lines
