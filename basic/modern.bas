#
# This is sample of BASIC code that will be transformed on the fly by
# the preprocessor to a classical program.
#
# The goal of this program is to demonstrate all the features that
# can be used to develop modern looking code that will be turned into
# efficient target code.
#

        low = 1
        high = 23

        cls
        print "Let's play, you have to guess a number."
        print "This number is between";low;"and";high

        delta = high - low + 1
        target = rnd(delta) + low


loop:   input "Your guess";guess

        if guess > target then gosub toohigh
        if guess < target then gosub toolow

        if guess = target then print "Congratulations !":end

        goto loop

toolow: print "Your guess is too low"
        return

toohigh: print "Your guess is too high"
         return
